package team.pi.app.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created on 2016/9/19
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class MsgServiceTest {

    private MsgService msgService;

    @Before
    public void init() {
        msgService = new MsgService();
    }

    @Test
    public void shouldReturnMessage() {
        String expected = "Hello Kitty!";
        Assert.assertEquals(expected, msgService.getMessage());
    }
}
