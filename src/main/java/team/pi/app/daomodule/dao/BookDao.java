package team.pi.app.daomodule.dao;

import team.pi.app.daomodule.entity.Book;

/**
 * Created on 2016/9/20
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public interface BookDao {

    Book find(Book book);
}
