package team.pi.app.daomodule.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pi.app.daomodule.dao.BookDao;
import team.pi.app.daomodule.entity.Book;
import team.pi.app.daomodule.mapper.BookMapper;

/**
 * Created on 2016/9/20
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
public class BookDaoImpl implements BookDao {

    @Autowired
    BookMapper bookMapper;

    @Override public Book find(Book book) {
        book.setName("Test Book");
        book.setDesc("Please fix this");
        return book;
//        return bookMapper.findById(book.getId());
    }
}
