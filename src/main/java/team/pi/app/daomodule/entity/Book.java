package team.pi.app.daomodule.entity;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

/**
 * Created on 2016/9/20
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */

@Getter
@Setter
public class Book {
    String id;
    String name;
    String num;
    String desc;
    Timestamp createTime;
    Timestamp updateTime;
}
