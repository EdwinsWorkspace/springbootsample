package team.pi.app.daomodule.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import team.pi.app.daomodule.entity.Book;

/**
 * Created on 2016/9/20
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
@Mapper
public interface BookMapper {

    @Select("SELECT * FROM pi_book_tbl where id = #{id}")
    Book findById(@Param("id") String id);
}
