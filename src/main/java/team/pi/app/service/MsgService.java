package team.pi.app.service;

import org.springframework.stereotype.Service;

/**
 * Created on 2016/9/19
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */

@Service
public class MsgService {

    public String getMessage() {
        return "Hello Kitty!";
    }
}
