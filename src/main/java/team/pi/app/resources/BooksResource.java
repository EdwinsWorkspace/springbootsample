package team.pi.app.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pi.app.businesslogic.BookBusinessLogic;
import team.pi.app.daomodule.entity.Book;
import team.pi.app.service.MsgService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created on 2016/9/19
 * [Function]
 * Simple Resource to response GET method
 *
 * @author zhangshuai
 * @version 1.2.1
 * @since 1.2.1
 */

@Component
@Path("/books")
public class BooksResource {

    @Autowired
    MsgService msgService;

    @Autowired
    BookBusinessLogic bookBusinessLogic;

    @GET
    public String message(){
        return msgService.getMessage();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBook(){
        Book book = new Book();
        return Response.ok().entity(bookBusinessLogic.findBook(book)).build();
    }
}
