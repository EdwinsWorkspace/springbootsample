package team.pi.app.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;
import team.pi.app.resources.BooksResource;

/**
 * Created on 2016/9/19
 * [Function]
 * Jersey config
 *
 * @author zhangshuai
 * @version 1.2.1
 * @since 1.2.1
 */
@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(BooksResource.class);
    }
}
