package team.pi.app.businesslogic.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pi.app.businesslogic.BookBusinessLogic;
import team.pi.app.daomodule.dao.BookDao;
import team.pi.app.daomodule.entity.Book;

/**
 * Created on 2016/9/20
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
public class BookBusinessLogicImpl implements BookBusinessLogic{

    @Autowired
    BookDao bookDao;

    @Override public Book findBook(Book book) {
        return bookDao.find(book);
    }
}
