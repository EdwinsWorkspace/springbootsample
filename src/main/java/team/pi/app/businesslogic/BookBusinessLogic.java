package team.pi.app.businesslogic;

import team.pi.app.daomodule.entity.Book;

/**
 * Created on 2016/9/20
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public interface BookBusinessLogic {

    Book findBook(Book book);
}
